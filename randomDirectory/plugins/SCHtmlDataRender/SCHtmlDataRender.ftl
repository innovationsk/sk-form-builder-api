<#assign alternatedata = "Enter the HTML Data which you need to view">
<#if ftlWidgetProperties??>
    <#assign widgetname = ftlWidgetProperties.widgetname>
    <#assign htmldata = ftlWidgetProperties.htmldata>
</#if>


<div class="staticWidget ${widgetname}" >
    <#if htmldata?? && htmldata?has_content>
        ${htmldata}
    <#else>
        ${alternatedata}
    </#if>
</div>
