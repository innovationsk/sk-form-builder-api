/*
 * (c)2002-2015 Skava. All rights reserved. The Skava system, including without
 * limitation, all software and other elements thereof, are owned or controlled
 * exclusively by Skava and protected by copyright, patent, and other laws. Use
 * without permission is prohibited. For further information contact Skava at
 * info@skava.com.
 */
var FieldPropertyWidget = StudioWidgetV2.extend(
{
    /*
     * Triggered when initializing a widget and will have the code that invokes rendering of the widget
     * setParentContainer(JQueryParentContainerDOM) - binds event to this container
     * setItemContainer(JQueryItemContainerDOM) - binds studio item events for respective item containers
     * bindEvents() - binds the studio event to this widget
     */
    accordionPanel: null,
    fieldContentPanel : null,
    init: function()
    {
        var thisObj = this;
        thisObj._super.apply(thisObj, arguments);
        thisObj.render();
        if ((typeof(Studio) != "undefined") && Studio && (typeof(readerObj) == "undefined"))
        {  
            $('.cls_skLeftPanelPlus').css({'pointer-events': 'none'});
        }
    },
    /*
     * Triggered from init method and is used to render the widget
     */
    getResponse:function(jsonurl, parent, returnValue){
        var thisObj = this;
            var obj = {
                url : jsonurl,
                type : "GET",
                contentType:'application/json',
                dataType: 'json',
                crossDomain: true,
                async:true,
                success : function(data){
                thisObj.accordionPanel = new SKAccordionPanel(parent, data, returnValue);
                },
                error : function(data, error){
                    console.log("Ajax Call error");
                }
            };
            $.ajax(obj);
    },
    render: function()
    {
        var thisObj = this;
        var widgetProperties = thisObj.getProperties();
        var elem = thisObj.getContainer();
        var items = thisObj.getItems();
        var connectorProperties = thisObj.getConnectorProperties();
        /*
         * API to get base path of your uploaded widget API file
         */
        var jsonurl = widgetProperties.JSONUrl;
        var typelistFlag = widgetProperties.type_list_flag;
        var widgetBasePath = thisObj.getWidgetBasePath();
        if (elem != undefined && elem != null && jsonurl != null)
        {
            var parent = $('<div>', {class : 'cls_skLeftPanelPlus wholeWrapper'}).appendTo(elem);
            var returnValue = function(parent, value, dataConfig) {
                var thisObj = this;
                window.scrollTo(0,0);
                thisObj.fieldContentPanel = new SKFieldDb(parent, value, dataConfig, typelistFlag);
                
            };
            thisObj.getResponse(jsonurl, parent, returnValue);
        }
        /*
         * API to bind global events to the item DOM :
         *  thisObj.sksBindItemEvent();
         * 
         * API to bind item events to the item DOM :
         *  thisObj.sksBindItemEvent(JQueryItemContainerSelector, ItemIdx);
         * JQueryItemContainerSelector - A JQuery selector that returns an array of DOMs that represents the individual item inside the item container, to which the hotspot needs to be bound.
         * ItemIdx (Optional) - To bind the item events to a specific item.
         */
        thisObj.sksBindItemEvent();
        /*
         * API to refresh the previously bound events when a resize or orientation change occurs.
         *  thisObj.sksRefreshEvents(ItemIdx);
         * ItemIdx (Optional) - To refresh events for a specific item. Default value is 0.
         */
        $(window).resize(function()
        {
            thisObj.sksRefreshEvents();
        });
    },
});