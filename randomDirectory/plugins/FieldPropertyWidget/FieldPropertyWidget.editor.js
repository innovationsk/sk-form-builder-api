/*
 * (c)2002-2015 Skava. All rights reserved. The Skava system, including without
 * limitation, all software and other elements thereof, are owned or controlled
 * exclusively by Skava and protected by copyright, patent, and other laws. Use
 * without permission is prohibited. For further information contact Skava at
 * info@skava.com.
 */

FieldPropertyWidget = FieldPropertyWidget.extend(
{
    /*
     * Config to define Widget Properties to be rendered in the left pane
     */
    propertiesConfig: [
    {
        config: [
        {
            "type": "file",
            "name": "JSONUrl",
            "label": "Include your JSON here",
            "validation":
            {
                "rules":
                {
                    "url": true
                }
            }
        },
        {
            "type": "text",
            "name": "WidgetName",
            "label": "Widget Name",
            "value": "",
            "validation":
            {
                "rules":
                {}
            }
        },
		{
            "type": "checkbox",
            "name": "type_list_flag",
            "label": "Choose to show the Type list"
        }]
    }],

    /*
     * Triggered when the user Creates a new widget and used to initialize the widget properties
     */
    create: function(cbk)
    {
        if (cbk)
        {
            this._super();
            cbk();
        }
    }
});

var params = {};
params.hasMultipleItems = false;
params.hasAreaSpecificEvents = false;
Studio.registerWidget("FieldPropertyWidget", "Widget for Form Builder Field Properties", params);