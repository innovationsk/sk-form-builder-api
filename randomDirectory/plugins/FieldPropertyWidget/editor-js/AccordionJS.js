var SKAccordionPanel = function(parent, data, cbk) { 
    this.parent         = parent || 'body';
    this.dataConfig     = data || {};
    this.returnValue    = cbk;
    this.init(this.parent);
};
 
SKAccordionPanel.prototype = { 
        
    registerEvents : function() {
        
		var thisObj = this;
        thisObj.$accordContent = $('.cls_skAccordContent', thisObj.parent);
        	
        var $accordionEl  = thisObj.$accordContent.find('.cls_skAccordTitle'),
            $fieldItems   = $('.cls_skContentList .cls_skAccSubTitleName', thisObj.parent);
        var renderingAction = function(count) {
            thisObj.subMenuIndex[count] = thisObj.subMenuIndex[count] || 0;
            thisObj.$accordContent.add($('.cls_skAccSubTitleName', thisObj.$accordContent)).removeClass('active');
            thisObj.$accordContentActive =  $(thisObj.$accordContent[count]).addClass('active');            
            thisObj.$accordContentActive.find('.cls_skAccordSubTitle').slideDown('slow');
            
            returnOItem =  $(thisObj.$accordContentActive.find('.cls_skAccSubTitleName')[thisObj.subMenuIndex[count]]).addClass('active').attr('name');
            thisObj.returnValue(thisObj.parent, returnOItem, thisObj.dataConfig);
        };
        var doAccordionClickToOpen = function() {
            thisObj.menuIndex = $(this).closest(thisObj.$accordContent).index();
            thisObj.$accordContentActive.find('.cls_skAccordSubTitle').slideUp('slow');
            renderingAction(thisObj.menuIndex);
        };
        var doOnClickGetFieldItem =  function() {
            thisObj.$accordContentActive.find('.cls_skAccSubTitleName').removeClass("active");
            $(this).addClass("active");
            var returnOItem = $(this).attr('name');
            thisObj.subMenuIndex[thisObj.menuIndex] = $(this).index();
            thisObj.returnValue(thisObj.parent, returnOItem, thisObj.dataConfig);
        };
		thisObj.$accordContent.find('.cls_skAccordSubTitle').hide();
        renderingAction(thisObj.menuIndex);
        $accordionEl.on('click', doAccordionClickToOpen);   
        $fieldItems.off('click').on('click', doOnClickGetFieldItem);
    },
    createAccordion: function() {
        var thisObj = this;           
 
       /*Accordion Panel DOM Creating*/
        var drawAccordion = function(configdata) {
                var content = "<div class = 'cls_skTabList'><h2 class = 'cls_skOverview'>Overview</h2><div class = 'cls_skTabListMainTitle'>List of Field Properties</div>";           
                    content += "<div class = 'cls_skContentList'>";
                /* Accordion Menu Creating*/
                for(var count = 0, facetCount = configdata.facets.length,  i=0; count < facetCount; count++, i++) {                 
                    content += "<div class = 'cls_skAccordContent' index = '"+ i +"'><div class = 'cls_skAccordTitle cls_downIcon'>"+configdata.facets[count].name+"</div>";
                    var data = configdata.childrens.elementList.filter(function(obj) { 
                        return obj.mode === configdata.facets[count].mode;
                    });
                    content += "<ul class ='cls_skAccordSubTitle'>";
                    /* Accordion Submenu Creating*/
                    for(var j = 0, childLiCount = data.length; j < childLiCount; j++) {
                        var idxData = data[j].title.trim();
                        idxData = idxData.replace(" ", "_");
                        content += "<li name = '"+data[j].mode+"_"+idxData+"'class = 'cls_skAccSubTitleName "+data[j].menuicon_name+"' index = '"+ j +"'>"+data[j].title+"</li>";
                    }
                    content += "</ul></div>";
                }
                
            return content += "</div></div>";
        };
        $(thisObj.parent).html(drawAccordion(thisObj.dataConfig));  
 
    },
    render: function() {
        var thisObj = this;
        thisObj.createAccordion();
        thisObj.registerEvents();
    },
    init: function(){
        var thisObj                     = this;
        thisObj.subMenuIndex            = [];
        thisObj.menuIndex               = 0;
        thisObj.$accordContent          = '';
        thisObj.$accordContentActive    = '';
        thisObj.render();
    }
};