var SKFieldDb = function(parent, value, dataConfig, typeFlag){
    this.parent     = parent || 'body';
    this.dataConfig = dataConfig;
    this.typeFlag   = typeFlag;
    this.init(value);
};
SKFieldDb.prototype = {
    
    registerEvents : function() {
        var thisObj         = this,
			listLen         = thisObj.subFieldObj.type.length,
			lastScroll      = 0,
			newScrollVal    = 0,
            headerHgt       = $('.right_parent').outerHeight();
        
        var actionToChangeActive = function(index) {
            $('.cls_sklistCont div.active', this.parRight).removeClass('active');
            $('.cls_sklistCont div[index = '+ index +']', this.parRight).addClass('active');
        };
        var pageClick = function(e) {
            id_idx = $(this).attr('index');
            actionToChangeActive(id_idx);
            window.scrollTo(0, ($('#list_'+id_idx+'', this.parLeft).offset().top - headerHgt - 20));
        };
        var doOnScrollBoth = function() {
            for(num = 0; num < listLen; num++) {
                eleTopVal = $('#list_'+ num +'').offset().top; 
                newScrollVal = $(window).scrollTop();
                typeContHgt = $('#list_'+num+'').height();
                screenHgt = window.innerHeight;
                windowHgt = $(document).height() - screenHgt;
                
				if((typeContHgt < screenHgt) && (newScrollVal == windowHgt) && (num == (listLen - 1))) {
					actionToChangeActive(num);
				}
				else {
					if((eleTopVal - newScrollVal) < headerHgt + 40) {
						actionToChangeActive(num);
					}
				}
            }
            lastScroll = newScrollVal;
        };
        var scrollEvt = function() {
            boundUpVal = $('#list_0',this.parLeft).offset().top - $(window).scrollTop();
            
            if(boundUpVal < (headerHgt + 40)) {
                doOnScrollBoth();                
            }
            else {
                $('.cls_sklistCont div.active',this.parRight).removeClass('active');
            }
        };
        $(window).scroll(scrollEvt);
        
        $('.cls_sklistCont div',this.parRight).on('click', pageClick);
    },
    render : function() {
        var thisObj = this;
        if($('.cls_skLeftPanelPlus').length > 0 ) {
            thisObj.Left      = $('<div>', {class : 'cls_skcont'}).appendTo(thisObj.parent);            
            thisObj.parLeft   = $('<div>', {class : 'cls_skcontWrap'}).appendTo(thisObj.Left);
            thisObj.parRight  = $('<div>', {class : 'listWrap'}).appendTo(thisObj.Left);
        }
        var renderContandImage = function() {
            
             var mediaLen = thisObj.subFieldObj.media.length, 
             
                descDiv  = $('<div>', {class : 'cls_skdescDiv'}).appendTo(thisObj.parLeft),
                 title   = $('<div>', {class : 'cls_sktitle', text : thisObj.subFieldObj.title}).appendTo(descDiv),              
                 descStr = $('<div>', {class : 'cls_skdesc' , html : thisObj.subFieldObj.desc}).appendTo(descDiv);
            if(thisObj.config.childrens.terms != null && thisObj.config.childrens.terms != undefined) {
                termsStr = $('<div>', {class : 'cls_skterms' , html : thisObj.config.childrens.terms}).appendTo(descDiv);
            }
            
            
            mediaDiv = $('<div>', {class : 'cls_skmediaDiv'}).appendTo(thisObj.parLeft);
            for(value = 0; value < mediaLen; value++) {
                mediaImgWrap = $('<div>', {class : 'cls_skmediaImg'}).appendTo(mediaDiv);
                mediaImg     = $('<img>', {src : thisObj.subFieldObj.media[value].url, 
                    class : 'cls_skImg' , 
                    alt : ''+thisObj.subFieldObj.media[value].name+'Representing Image'}).appendTo(mediaImgWrap);
            }
        };        
        var renderTypeContent = function() {
            var typeLen = thisObj.subFieldObj.type.length;
                typeDiv = $('<ol>', {class : 'cls_sktypeDiv'}).appendTo(thisObj.parLeft);
            for(num = 0; num < typeLen; num++) {
                
                typeCont  = $('<li>', {class : 'cls_sktypeCont', id : 'list_'+num+''}).appendTo(typeDiv);
                typeEachNameDiv = $('<div>', {class : 'cls_sktypeEach'}).appendTo(typeCont);
                typeTitle = $('<span>', {class : 'cls_sktypeTitle', text : thisObj.subFieldObj.type[num].value}).appendTo(typeEachNameDiv);
                typeDesc  = $('<span>', {class : 'cls_sktypeDesc', text : thisObj.subFieldObj.type[num].contentDesc}).appendTo(typeEachNameDiv);
                
            }            
        };
        var renderTypeMenuList = function() {
            var listLen  = thisObj.subFieldObj.type.length, 
                rightPan = $('<div>', {class : "cls_sklistWrap"}).appendTo(thisObj.parRight),
				typeName = "Categories";
				typeName = typeName.trim();
            
            list     = $('<div>', {class : "cls_sklistDiv"}).appendTo(rightPan);
            title    = $('<div>', {class : "cls_sklistTitle", text : typeName}).appendTo(list);
            menulistDiv = $('<ul>', {class : "cls_sklistheadDiv"}).appendTo(list);
            for(val = 0; val < listLen; val++) {            
                menuList = $('<li>', {class : "cls_sklistCont"}).appendTo(menulistDiv);
                menuListEach = $('<div>', {class : "cls_sklistEach" , text : thisObj.subFieldObj.type[val].value}).appendTo(menuList);
                $(menuListEach).attr('index' , val);
            }
        };
        var typeDecision = function() {
            if(thisObj.typeListEnable == "true") {
                var documentHeight  = $(document).height(),
                    windowHeight    = window.innerHeight;
                
                if( documentHeight  > windowHeight ) {
                    
                    renderTypeMenuList();
                    var typeLsitDomWidth = $(thisObj.parRight).width() || 0;
                    $(thisObj.Left).css({ 'width' : 'calc(100% - 280px)'});
                }    
                else {
                    $(thisObj.parRight).remove();
                    $(thisObj.Left).css({ 'width' : '100%' });
                }            
            }
            else {
                $(thisObj.Left).css({ 'width' : '100%' });
                $(thisObj.parRight).remove();
            }
        };
        if(thisObj.subFieldObj != undefined && thisObj.subFieldObj != null) {
            renderContandImage();
            renderTypeContent();
            typeDecision();
        }
    },
    resetData : function(data) { 
        var thisObj = this,                        
            str     = data,
            strArr  = [];
    
       var getDatadecode = function()  {
            strArr      = str.replace("_"," ").split(" ");
            mode        = strArr[0];  
            fieldName   = strArr[1].replace("_"," ");          
       };
       var compareAndGetField = function() {
            
            if (thisObj.config != undefined && thisObj.config != null && 
                thisObj.config.childrens != undefined && thisObj.config.childrens != null) {
                
                for(count = 0, len = thisObj.config.childrens.elementList.length; count < len ; count++ ) {
                    
                    getTitleConfig = thisObj.config.childrens.elementList[count].title.trim();
                    if(getTitleConfig == fieldName) {
                        thisObj.subFieldObj = thisObj.config.childrens.elementList[count];
                        console.log(thisObj.subFieldObj);
                    }
                }
            }
       };
       getDatadecode();
       compareAndGetField();
    },
    init : function(fieldVal) {
        var thisObj             = this;
        thisObj.parLeft         = "";
        thisObj.parRight        = "";
        thisObj.contentPerent   = "";
        thisObj.parent          = this.parent;
        thisObj.config          = this.dataConfig;
        thisObj.typeListEnable  = thisObj.typeFlag;
        $('.cls_skcont', thisObj.parent).remove();
        $('.listWrap', thisObj.parent).remove();
        
        if(typeof(fieldVal) != undefined) {
            thisObj.resetData(fieldVal);    
            thisObj.render();
            thisObj.registerEvents();
        }
        else {
            var errorDisp = $('<div>', {class : "cls_skerror" , text : "field data hasn't recieved"}).appendTo($(thisObj.parent));
        }
    }
};