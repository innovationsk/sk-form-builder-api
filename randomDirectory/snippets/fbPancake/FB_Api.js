function SKFBHeaderPanel(parent) {
    this.parent = parent || "body";
    this.pageNavigation = "guide";
    this.init();
    this.leftMenu = 70;
};

SKFBHeaderPanel.prototype = {

    init: function(){
        var thisObj= this;

        thisObj.breadCrumb(this.pageNavigation);
        thisObj.registerEvents();
    },    
	registerEvents: function(){

            var thisObj    = this,
                pancakeEl   = $(".pancake"),
                rightPanel = $(".right_parent"), 
                tabEl    = $(".pc_list");

                $(tabEl[0]).addClass("selected");
            
            var clickAction = function() {

                var thisEl   = this,
                    parentEl = $(thisEl).closest('.pancake');

                $(tabEl, parentEl).removeClass("selected");
                $(this).addClass("selected");

                var nameEl = $(this).attr('name');
                thisObj.search(nameEl);

                thisObj.pageNavigation = $(this).text().trim();

                thisObj.breadCrumb(thisObj.pageNavigation);
            };

            var hoverAction = function(){
               $(this).css("width", "350px");
                $(".right_parent").css({"margin-left":"350px",
                    "width":"calc(100% - 350px)"
                });
                $(".cls_skLeftPanelPlus").css({"margin-left":"350px",
                    "transition":"all 0.5s ease",
                    "width": "calc(100% - 350px)"
                });
                $(".cls_skTabList, .listWrap").css("width", "calc((100% - 350px)/4)");
            };

            var hoverLeave = function(){
                $(this).css("width", "70px");
                $(".right_parent").css({"margin-left":"70px",
                    "width":"calc(100% - 70px)"
                });
                $(".cls_skLeftPanelPlus").css({"margin-left":"70px",
                    "transition":"all 0.5s ease",
                     "width": "calc(100% - 70px)"
                });
                $(".cls_skTabList, .listWrap").css("width", "calc((100% - 70px)/4)");
            };

            tabEl.on('click', clickAction);
            pancakeEl.mouseenter(hoverAction);
            pancakeEl.mouseleave(hoverLeave);
    
    },
    search: function(nameEl)
    {
        var thisObj = this;
        if(nameEl == "fieldproperties" || nameEl == "api") {
            $(".search_bar").css("display","inline-block");
        }
        else {
            $(".search_bar").css("display","none");
        }

        $(".search").on('change', function(){
            var text = $(this).val();
            console.log(text);
            

        });
    },
    breadCrumb: function(val) {
        var thisObj = this;

        $(".breadcrumb_home").remove();
        if($(".pc_list_title").children().hasClass('list_title'))
        {
            $(".list_title").remove();
        }
        $('<div>', { class: 'list_title', text: val} ).prependTo($(".pc_list_title"));
        var cont = $('<span>', { class: 'breadcrumb_home', text: "Home"} ).appendTo($(".breadcrumb"));
        $('<span>', { class: 'breadcrumb_title', text: " / "+val} ).appendTo($(cont));
    }

};

$(document).ready(function() {
    var parent = $(".pancake");
    var obj = new SKFBHeaderPanel(parent);
});