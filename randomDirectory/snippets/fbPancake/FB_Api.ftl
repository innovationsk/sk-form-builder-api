<#if ftlFacets??> 
    <#assign values = ftlFacets.value> 
    <#assign logo = ftlFacets.logo> 
</#if>

<#macro splitFunc value>
    <#assign icon = "" />
    
    <#list value?split("|") as values>
        <#assign icon = values?trim >
        <#assign iconurl = icon?replace(" ","")>

        <div class = "pc_list" name = "${iconurl}">
            <span class = "${iconurl}_icon"></span>
            <label for = "pc_list_text">${icon}</label>
        </div>

    </#list>
    
</#macro>

        
<div class = "left_parent">
    <div class = "pancake">

        <div class = "header">
            <div class = "fb_logo" style = "background-image : url(${logo})"></div>
            <h2 class ="fb_title">FormBuilder</h2>
        </div>

        <div class = "pc_cont">
            <div class = "pc_data">
            	<@splitFunc values/>
            </div>
        </div>

    </div>
</div>

<div class = "right_parent">

    <div class = "left_cont">
        <div class = "pc_list_title"></div>
        <div class = "breadcrumb"></div>
    </div>

    <div class = "right_cont">
        <div class= "search_bar">
            <input class ="search" type="search" />
            <span class ="search_icon"></span>
        </div>

        <div class = "download_button">
            <div class = "download">
                <div class = "downloadtext">
                    <label for = "download">DOWNLOAD</label>
                    <span class = "custom_text">Your customized code</span>
                </div>
                <span class = "download_icon"></span>
            </div>
        </div>
    </div>
</div>